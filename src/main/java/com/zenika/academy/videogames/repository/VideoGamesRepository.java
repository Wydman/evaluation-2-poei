package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        Optional<VideoGame> optionalVideoGame = Optional.ofNullable(this.videoGamesById.get(id));
        return optionalVideoGame;
    }

    public void save(VideoGame videoGame) {
        this.videoGamesById.put(videoGame.getId(), videoGame);
    }

    public void delete(Long id) {
        if(videoGamesById.get(id)!=null){
            this.videoGamesById.remove(id);
        }
    }
}
