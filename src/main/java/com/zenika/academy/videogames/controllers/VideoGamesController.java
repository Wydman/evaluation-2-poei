package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.domain.VideoGameNotFoundException;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;
    private RawgDatabaseClient rawgDatabaseClient;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService, RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesService = videoGamesService;
        this.rawgDatabaseClient = rawgDatabaseClient;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) throws VideoGameNotFoundException {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if(foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/Delete/{id}")
    public void deleteOneVideoGame(@PathVariable("id") Long id) throws VideoGameNotFoundException {
        videoGamesService.deleteVideoGame(id);
    }


    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */


    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) throws VideoGameNotFoundException {
       Optional<VideoGame>  optionalVideoGame = Optional.of(this.videoGamesService.addVideoGame(videoGameName.getName(),rawgDatabaseClient));
       return Optional.of(this.videoGamesService.addVideoGame(videoGameName.getName(),rawgDatabaseClient)).orElseThrow(VideoGameNotFoundException::new);

    }


    /*
    @PostMapping
    public ResponseEntity addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
       return Optional.of(this.videoGamesService.addVideoGame(videoGameName.getName(),rawgDatabaseClient))
               .map(videoGame -> ResponseEntity.ok(videoGame))
               .orElse(ResponseEntity.notFound().build());
    }
     */
}
