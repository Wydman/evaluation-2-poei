package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.domain.VideoGameNotFoundException;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository) {
        this.videoGamesRepository = videoGamesRepository;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id).orElse(null);
        // return this.videoGamesRepository.get(id).orElseThrow(VideoGameNotFoundException::new);
    }
    public void deleteVideoGame(Long id) {
        videoGamesRepository.delete(id);
    }

    public VideoGame addVideoGame(String name, RawgDatabaseClient rawgDatabaseClient) throws VideoGameNotFoundException {
        Optional<VideoGame> optionalNewGame = Optional.ofNullable(rawgDatabaseClient.getVideoGameFromName(name));
        //  return videoGamesRepository.save(optionalNewGame.orElse(null));
        // return videoGamesRepository.save(Optional.of(
        //       rawgDatabaseClient.getVideoGameFromName(name))
        //     .orElse(rawgDatabaseClient.getVideoGameFromName("404")));
        videoGamesRepository.save(optionalNewGame.orElse(rawgDatabaseClient.getVideoGameFromName("404")));
        return optionalNewGame.orElse(rawgDatabaseClient.getVideoGameFromName("404"));
       // return optionalNewGame.orElse(rawgDatabaseClient.getVideoGameFromName("404").orElseThrow(null));
    }
}
