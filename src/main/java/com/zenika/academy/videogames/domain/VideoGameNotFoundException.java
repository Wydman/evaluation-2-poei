package com.zenika.academy.videogames.domain;

public class VideoGameNotFoundException extends Exception {
    public VideoGameNotFoundException(){
        System.out.println("Video Game Not Found !");
    }
}
