package com.zenika.academy.videogames;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.domain.VideoGameNotFoundException;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.PostConstruct;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class VideoGamesCollectionApplicationTests {
    @Test
    void contextLoads() {
    }
    @Test
    public void TestVideoGamesRepository() {
        VideoGamesRepository videoGamesRepository = new VideoGamesRepository();
        RawgDatabaseClient mockDatabaseClient = Mockito.mock(RawgDatabaseClient.class);
        Mockito.when(mockDatabaseClient.getVideoGameFromName("Doom"))
                .thenReturn(new VideoGame(1L, "Doom", new ArrayList<Genre>()));
        Mockito.when(mockDatabaseClient.getVideoGameFromName("MassEffect"))
                .thenReturn(new VideoGame(234L, "MassEffect", new ArrayList<Genre>()));
        Mockito.when(mockDatabaseClient.getVideoGameFromName("Suikoden2"))
                .thenReturn(new VideoGame(8L, "Suikoden2", new ArrayList<Genre>()));

        Assertions.assertTrue(videoGamesRepository.get(1L).isEmpty());

        videoGamesRepository.save(mockDatabaseClient.getVideoGameFromName("Doom"));
        videoGamesRepository.save(mockDatabaseClient.getVideoGameFromName("MassEffect"));

        Assertions.assertEquals(2, videoGamesRepository.getAll().size());

        videoGamesRepository.save(mockDatabaseClient.getVideoGameFromName("Suikoden2"));

        Assertions.assertTrue(videoGamesRepository.get(1L).isPresent());
        Assertions.assertTrue(videoGamesRepository.get(9L).isEmpty());

        Assertions.assertEquals(3, videoGamesRepository.getAll().size());
    }

    @Test
    public void TestVideoGamesService() throws VideoGameNotFoundException {
        VideoGamesRepository videoGamesRepository = new VideoGamesRepository();
        VideoGamesService videoGamesService = new VideoGamesService(videoGamesRepository);
        RawgDatabaseClient mockDatabaseClient = Mockito.mock(RawgDatabaseClient.class);


        Mockito.when(mockDatabaseClient.getVideoGameFromName("Doom"))
                .thenReturn(new VideoGame(1L, "Doom", new ArrayList<Genre>()));
        Mockito.when(mockDatabaseClient.getVideoGameFromName("NierAutomata"))
                .thenReturn(new VideoGame(2L, "NierAutomata", new ArrayList<Genre>()));
        Mockito.when(mockDatabaseClient.getVideoGameFromName("MassEffect"))
                .thenReturn(new VideoGame(234L, "MassEffect", new ArrayList<Genre>()));
        Mockito.when(mockDatabaseClient.getVideoGameFromName("Suikoden2"))
                .thenReturn(new VideoGame(8L, "Suikoden2", new ArrayList<Genre>()));

        Assertions.assertEquals(videoGamesService.addVideoGame("NierAutomata",mockDatabaseClient).getName(),videoGamesService.getOneVideoGame(2L).getName());

        videoGamesService.addVideoGame("Doom",mockDatabaseClient);
        videoGamesService.addVideoGame("MassEffect",mockDatabaseClient);
        videoGamesService.addVideoGame("Suikoden2",mockDatabaseClient);

        Assertions.assertEquals(videoGamesService.ownedVideoGames().size(), videoGamesRepository.getAll().size());
    }
}
